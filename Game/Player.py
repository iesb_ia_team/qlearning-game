import copy
import random
import operator


class Player:
    def __init__(self, level, learning_rate: float, discount: float, log_level: str, exploration_steps: int):
        self.level = level
        self.learning_rate = learning_rate
        self.discount = discount
        self.random_rate = 100
        self.random_rate_decrease = 100 / exploration_steps
        self.log_level = log_level

        self.q_table = []
        self.__generate_initial_q_table(level)

        self.position = {}
        self.go_to_initial_position(level)

    def take_a_move(self):
        if self.at_finish():
            self.go_to_initial_position(self.level)

        x_position = self.position.get('x')
        y_position = self.position.get('y')

        move = self.__choose_a_move()

        print('Move to', move) if self.log_level in ['DEBUG', 'VERBOSE'] else None

        if move == 'up':
            self.position.update({'y': self.position.get('y') - 1})
        if move == 'down':
            self.position.update({'y': self.position.get('y') + 1})
        if move == 'left':
            self.position.update({'x': self.position.get('x') - 1})
        if move == 'right':
            self.position.update({'x': self.position.get('x') + 1})

        q_learning_value = self.__bellman_equation_calc(move, x_position, y_position)

        self.q_table[y_position][x_position].update({move: round(q_learning_value, 5)})

        print('Q-table actions:', self.q_table[y_position][x_position]) if self.log_level in ['VERBOSE'] else None

        if self.random_rate > 0:
            self.random_rate -= self.random_rate_decrease

        return True

    def __bellman_equation_calc(self, move, x_position, y_position):
        current_q_table_value = self.q_table[y_position][x_position].get(move)
        new_position = self.level[self.position.get('y')][self.position.get('x')]
        reward = new_position.reward
        estimated_next_reward = max(self.__possible_moves().items(), key=operator.itemgetter(1))[1]
        learning_rate = self.learning_rate
        discount = self.discount
        q_learning_value = current_q_table_value + learning_rate * (reward + discount * estimated_next_reward - current_q_table_value)
        return q_learning_value

    def at_finish(self):
        return self.level[self.position.get('y')][self.position.get('x')].tile_type == 'finish'

    def __possible_moves(self):
        row = self.position.get('y')
        col = self.position.get('x')
        actions = {}
        if self.level[row - 1][col].accessible:
            actions.update({'up': self.q_table[row][col].get('up')})

        if self.level[row + 1][col].accessible:
            actions.update({'down': self.q_table[row][col].get('down')})

        if self.level[row][col - 1].accessible:
            actions.update({'left': self.q_table[row][col].get('left')})

        if self.level[row][col + 1].accessible:
            actions.update({'right': self.q_table[row][col].get('right')})

        return actions

    def __choose_a_move(self):
        move_type = random.choice(['best_move'] * (100 - int(self.random_rate)) + ['random'] * int(self.random_rate))

        if max(self.__possible_moves().items(), key=operator.itemgetter(1))[0] == min(self.__possible_moves().items(), key=operator.itemgetter(1))[0]:
            move_type = 'random'

        if move_type == 'best_move':
            print('Move type: best') if self.log_level in ['DEBUG', 'VERBOSE'] else None
            return max(self.__possible_moves().items(), key=operator.itemgetter(1))[0]
        else:
            print('Move type: random') if self.log_level in ['DEBUG', 'VERBOSE'] else None
            return random.choice(list(self.__possible_moves().keys()))

    def go_to_initial_position(self, level):
        for row in range(len(level)):
            for col in range(len(level[row])):
                if level[row][col].tile_type == 'start':
                    self.position = {'x': col, 'y': row}

    def __generate_initial_q_table(self, level):
        self.q_table = copy.deepcopy(level)
        for row in range(len(self.q_table)):
            for col in range(len(self.q_table[row])):
                self.q_table[row][col] = {'up': 0, 'down': 0, 'left': 0, 'right': 0}

