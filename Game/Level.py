from Game.Tile import Wall, Prize, Start, Finish, Path, Danger


class Level:

    @staticmethod
    def import_level(level_address):
        start = 0
        finish = 0
        file = open(level_address, 'r').readlines()

        level_map = []
        for row in file:
            line = []
            for col in row:
                if col == 'w':
                    line.append(Wall())
                elif col == 'd':
                    line.append(Danger())
                elif col == 'p':
                    line.append(Path())
                elif col == 'f':
                    finish += 1
                    line.append(Finish())
                elif col == 's':
                    start += 1
                    line.append(Start())
                elif col == '$':
                    line.append(Prize())

            level_map.append(line)

        if finish == 0:
            raise Exception('Finish not found')

        if start == 0:
            raise Exception('Start not found')

        return level_map

