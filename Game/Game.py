from Game.Level import Level
from Game.Player import Player
from Game.ScreenControl import ScreenControl

import time


class Game:
    def __init__(self, level_address: str, exploration_steps: int, log_level: str, learning_rate: float, discount: float):

        self.level = Level.import_level(level_address)
        self.log_level = log_level

        self.player = Player(self.level, learning_rate, discount, log_level, exploration_steps)

        for i in range(exploration_steps):
            step = str((100*i)/exploration_steps)+'%'
            self.iteration(step)

        self.player.go_to_initial_position(self.level)
        self.player.random_rate = 0
        for i in range(100):
            self.iteration()
            time.sleep(0.5)
            if self.player.at_finish():
                print('Finished with', str(i + 1), 'moves.') if log_level in ['INFO', 'DEBUG', 'VERBOSE'] else None
                break

        ScreenControl.print_q_table_map(self.level, self.player.q_table)

        ScreenControl.print_q_table(self.player.q_table) if log_level in ['DEBUG', 'VERBOSE'] else None

    def iteration(self, step=None):
        ScreenControl.clear()
        print('Learning.....', step) if step and self.log_level in ['INFO', 'DEBUG', 'VERBOSE'] else None
        while not self.player.take_a_move():
            pass
        ScreenControl.print_map(self.level, self.player) if self.log_level in ['INFO', 'DEBUG', 'VERBOSE'] else None
