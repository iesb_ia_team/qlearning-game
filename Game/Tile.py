from Game.ScreenControl import ScreenControl, Color


class Tile:
    def __init__(self, tile_type, representation, color, accessible=True, reward=0):
        self.tile_type = tile_type
        self.accessible = accessible
        self.reward = reward
        self.color = color
        self.representation = representation

    def __repr__(self):
        return ScreenControl.color_print(self.representation, color=self.color)


class Wall(Tile):
    def __init__(self):
        Tile.__init__(self, 'wall', '#', Color.WHITE, accessible=False)


class Path(Tile):
    def __init__(self):
        Tile.__init__(self, 'path', ' ', Color.GREEN)


class Danger(Tile):
    def __init__(self):
        Tile.__init__(self, 'danger', '~', Color.RED, reward=-50)


class Prize(Tile):
    def __init__(self):
        Tile.__init__(self, 'prize', '$', Color.YELLOW, reward=50)


class Start(Tile):
    def __init__(self):
        Tile.__init__(self, 'start', '@', Color.CYAN)


class Finish(Tile):
    def __init__(self):
        Tile.__init__(self, 'finish', '@', Color.PURPLE, reward=250)


