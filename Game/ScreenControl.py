import copy
import operator
import os
from enum import Enum

from Game.Player import Player


class Color(Enum):
    BLACK = '30'
    RED = '31'
    GREEN = '32'
    YELLOW = '33'
    BLUE = '34'
    PURPLE = '35'
    CYAN = '36'
    WHITE = '37'


class Background(Enum):
    BLACK = '40'
    RED = '41'
    GREEN = '42'
    YELLOW = '43'
    BLUE = '44'
    PURPLE = '45'
    CYAN = '46'
    WHITE = '47'
    NONE = '48'


class Style(Enum):
    NONE = '0'
    BOLD = '1'
    UNDERLINE = '2'
    NEGATIVE = '3'
    BLINK = '5'


class ScreenControl:
    @staticmethod
    def clear():
        if os.name == 'posix':
            os.system('clear')
        else:
            os.system('cls')

    @staticmethod
    def color_print(text: str, color: Color = Color.BLACK, style: Style = Style.NONE, background: Background = Background.NONE):
        return '\033[' + style.value + ';' + color.value + ';' + background.value + 'm' + text

    @staticmethod
    def print_map(level: list, player: Player):
        t = ''
        for row in range(len(level)):
            for col in range(len(level[row])):
                if player and row == player.position.get('y') and col == player.position.get('x'):
                    t += ScreenControl.color_print('X', color=Color.GREEN, style=Style.BOLD)
                else:
                    t += repr(level[row][col])
            t += '\n'
        print(t)

    @staticmethod
    def print_q_table_map(level: list, q_table: list):
        best_move_map = copy.deepcopy(level)
        for row in range(len(q_table)):
            for col in range(len(q_table[row])):
                _max = max(q_table[row][col].items(), key=operator.itemgetter(1))[0]
                _min = min(q_table[row][col].items(), key=operator.itemgetter(1))[0]

                if level[row][col].tile_type == 'wall':
                    best_move_map[row][col] = '#'
                elif _max == _min:
                    best_move_map[row][col] = 'X'
                else:
                    best_move = _max

                    if best_move == 'up':
                        best_move_map[row][col] = u'\u2191'
                    if best_move == 'down':
                        best_move_map[row][col] = u'\u2193'
                    if best_move == 'left':
                        best_move_map[row][col] = u'\u2190'
                    if best_move == 'right':
                        best_move_map[row][col] = u'\u2192'

        t = ''
        for row in range(len(best_move_map)):
            for col in range(len(best_move_map[row])):
                t += ScreenControl.color_print(best_move_map[row][col], color=level[row][col].color)
            t += '\n'

        print(t)

    @staticmethod
    def print_q_table(q_table):
        print('ROW\tCOL\tSTATE')
        for row in range(len(q_table)):
            for col in range(len(q_table[row])):
                print(str(row), '\t', str(col), '\t',  q_table[row][col])




