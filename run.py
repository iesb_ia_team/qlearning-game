from Game.Game import Game
import argparse

parser = argparse.ArgumentParser()

# Args
parser.add_argument("level_path", help="path of the level", type=str)
parser.add_argument("exploration_steps", help="quantity of steps trying learn the better path", type=int)

# Optional
parser.add_argument('--version', action='version', version='QLearning-Game 1.1')
parser.add_argument("--learning_rate", help="percentage of learning rate", type=float, default=0.1)
parser.add_argument("--discount", help="percentage of discount", type=float, default=0.95)
parser.add_argument("-l", "--log_level", help="log level", choices=['NONE', 'INFO', 'DEBUG', 'VERBOSE'], default='INFO')

args = parser.parse_args()

print('Parameters of execution: ', args) if args.log_level in ['VERBOSE'] else None

Game(args.level_path, args.exploration_steps, args.log_level, args.learning_rate, args.discount)

