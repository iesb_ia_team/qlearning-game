# QLearning-Game

## 1. Q-learning

O Q-learning é um algoritmo de aprendizado por reforço sem modelo. O objetivo do Q-learning é aprender uma política, que diz a um agente qual ação tomar sob quais circunstâncias. Ele não requer um modelo do ambiente, e pode lidar com problemas com transições e recompensas estocásticas, sem exigir adaptações.

## 2. QLearning-Game

É um jogo simples onde um agente tenta descobrir a forma mais vantajosa para sair de um labirinto. Após N passos *(default: 10.000)*, o agente mostra a política que descobriu e tenta sair do labirinto.

### 2.1 Q-table

Nossa Q-table é uma matriz de 3 dimensões, onde *x* e*y* representam as coordenadas de cada estado possível e *z* representa as possíveis ações com seus respectivos possiveis rewards *{up:0, down:0, left:0, rigth:0}*.  

A cada passo do agente, um dos valor q-table é atualizado. Espera-se que no final dos N passos a sequencia dos maiores valores levem ao maior reward.

### 2.2 Forma de representação 

Foi utilizado um gráfico simples feito apenas com ASCII, visando diminuir a complexidade. 

#### 2.2.1 Exemplo do mapa

A cada iteração é mostrado o movimento que o agente fez.

    #######  # = Parede
    #   ~##  X = Agente
    # ~  ~#  ~ = Perigo (reward negativo)
    #@#~ X#  @ = Alvo (reward positivo)
    #######

#### 2.2.2 Debug

Durante a fase de exploitation é possível acompanhar:    
- **1º linha**: Escolha tipo de movimento feito pelo agente (*best move* ou *random move*) ;  
- **2º linha**: Direção do movimento (*up*, *down*, *left*, *rigth*) ;  
- **3º linha**: Valores da Q-table do estado de início daquele passo ;  
- **4º linha**: Valores da Q-table do estado de fim daquele passo.  
  
Exemplo:

    best move  
    down  
    {'up': 225.62487, 'down': 249.99995, 'right': 175.62487}  
    {'up': 0}  

#### 2.2.3 Encerramento

Ao final do experimento é mostrada uma "animação" com o agente indo do ponto inicial ao ponto final, demonstrando o que aprendeu.  

Ainda é mostrado um novo mapa com a "tradução" visual dos melhores valor da q-table. Como o exemplo:

    #######
    #↓←←←##
    #↓←↑←←#
    #↑#↑↑←#
    #######

## 3. Como criar um novo mapa

Os mapas do nosso QLeraning-Game são muito simples, feitos apenas com caracteres ASCII e são convertidos para o mapa do jogo em tempo de execução.

### 3.1 Componentes do mapa

    w = Parede
    p = Caminho livre
    d = Perigo
    s = Ponto de início
    f = Ponto de fim
    $ = Prêmio menor ... péssima ideia... =(

### 3.2 Dimensões

É importante que as linhas e colunas do mapa devem ser de tamanho fixo. Novamente, todas as linhas devem ser do mesmo tamanho e todas as colunas devem ser do mesmo tamanho. Conforme o exemplo:

    wwwwwww       \        #######
    wpppdww        \       #   ~##
    wpdppdw   ------>      # ~  ~#
    wfwdpsw        /       #@#~ @#
    wwwwwww       /        #######

## 4. Executando

A execução é feita através do programa run.py, sendo o único argumento o endereço do mapa:

    python run.py level/lvl1

Não e necessária nenhuma biblioteca, nem um ambiente virtual. Todos os testes foram feitos utilizando **Python 3** e **Linux**, utilize outras configurações por conta própria. 

## 5. Conclusão

Foi possível observar a utilidade do Q-Leraning para cenários simples e após poquissimas iterações (< 250) a melhor política já estava definida.   

Notamos porém a dificuldade de encontrar uma boa politica nos seguinte casos:  
- Cenários muito grandes (> 20 x 20);  
- Mais aleatórios;  
- Com outros reward positivos;  
- Com um agente com muitas ações.  

